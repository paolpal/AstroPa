# AstroPa
Simulation of the **law of universal gravitation** in the **inner solar system**.

Simulazione della della **legge di gravitazione universale** nel **sistema solare interno**.

## Aggiornamento
Repo copiata da github.

### Test della pull
Ha funzionato.

![screenshot of the app](https://raw.githubusercontent.com/paolpal/AstroPa/master/img/screenshot.png)
